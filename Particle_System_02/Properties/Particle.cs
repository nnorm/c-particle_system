﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Particle_System_02
{
	public class Particle
	{
		public Vector3 Color = new Vector3(1.0f);
		public Vector3 Position = new Vector3(0.0f);
		public Vector3 Direction = new Vector3(0.0f);
		public float Lifetime = 1.0f;
		public float Size = 5.0f;
		public float Age = 0.0f;

		public Particle ()
		{

		}
	}
}

