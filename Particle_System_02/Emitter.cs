﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Particle_System_02
{
	public class Emitter
	{
		public Vector3 Position = new Vector3(0.0f);
		public Vector3 Extension = new Vector3 (1.0f);

		public Vector3 DirectionBase = new Vector3(0.0f);
		public Vector3 DirectionAlea = new Vector3(1.0f);

		public float Lifetime = 10.0f;
		public float LifetimeAlea = 0.0f;

		public int Rate = 0;

		private Random randomGenerator = new Random();

		public Vector3 Color = new Vector3(1.0f);


		public Emitter ()
		{
		}

		public List<Particle> Emit(int quantity)
		{
			List<Particle> newParticles = new List<Particle>();

			for (int i = 0; i < quantity; i++) 
			{
				Particle p = new Particle();

				Vector3 positionRelative = p.Position + Position;
				p.Position.X = Position.X - (float)(randomGenerator.NextDouble()) * (Extension.X * 2.0f) + Extension.X;
				p.Position.Y = Position.Y - (float)(randomGenerator.NextDouble()) * (Extension.Y * 2.0f) + Extension.Y;
				p.Position.Z = Position.Z - (float)(randomGenerator.NextDouble()) * (Extension.Z * 2.0f) + Extension.Z;

				p.Direction = new Vector3 (
					DirectionBase.X + (float)(randomGenerator.NextDouble() * 2.0 - 1.0) * DirectionAlea.X,
					DirectionBase.Y + (float)(randomGenerator.NextDouble() * 2.0 - 1.0) * DirectionAlea.Y,
					DirectionBase.Z + (float)(randomGenerator.NextDouble() * 2.0 - 1.0) * DirectionAlea.Z
				);
				p.Color = Color;

				p.Lifetime = Lifetime + (float)(randomGenerator.NextDouble () * 2.0 - 1.0) * LifetimeAlea;
				newParticles.Add (p);
			}
			return newParticles;
		}

		public bool Intersect(Vector3 particle)
		{
			Vector3 positionRelative = particle - Position;
			if ((Math.Abs (positionRelative.X)) > Extension.X || (Math.Abs (positionRelative.Y) > Extension.Y) || (Math.Abs (positionRelative.Z) > Extension.Z))
			{
				return false;
			}
		}

		void RenderDomain()
		{
			/* method to render the emitter domain */
		}
	}
}

