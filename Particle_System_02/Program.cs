﻿using System;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using OpenTK.Platform;

namespace Particle_System_02
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			NativeWindow window = new NativeWindow (800, 600, "Particle System", GameWindowFlags.FixedWindow, GraphicsMode.Default, DisplayDevice.Default);
			GraphicsContext context = new GraphicsContext (GraphicsMode.Default, window.WindowInfo, 4, 1, GraphicsContextFlags.Default);
			(context as IGraphicsContextInternal).LoadAll ();

			window.Visible = true;

			window.KeyDown += (sender, e) => 
			{
				if(e.Keyboard[Key.Escape])
					window.Close();
			};

			bool isExiting = false;

			string vertexShader = 
				@"
			#version 410 core
			layout(location=0) in vec3 attrPosition;

			uniform mat4 proj;
			uniform mat4 mv;
			uniform float time;
			out vec4 pos;

			void main()
			{
				gl_Position = pos = proj * mv * vec4(attrPosition, 1.0);
			}
			";

			string fragmentShader = 
				@"
			#version 410 core
			layout(location=0) out vec4 fragColor;
			uniform float time;
			in vec4 pos;
			void main()
			{
				vec4 baseColor = vec4(0.2,0.3,0.8,1.0);
				vec4 aleaColor = baseColor*tan(length(pos));
				fragColor = (mix(baseColor,aleaColor,cos(time*2.0)*0.75)*0.5 * mix(baseColor,aleaColor,sin(time*2.0)*0.75)*0.5);
			}
			";

			int renderShader = GL.CreateProgram ();
			int vertexshader = GL.CreateShader (ShaderType.VertexShader);
			GL.ShaderSource (vertexshader, vertexShader);
			GL.CompileShader (vertexshader);
			Console.WriteLine (GL.GetShaderInfoLog (vertexshader));
			GL.AttachShader (renderShader, vertexshader);
			int fragmentshader = GL.CreateShader (ShaderType.FragmentShader);
			GL.ShaderSource (fragmentshader, fragmentShader);
			GL.CompileShader (fragmentshader);
			Console.WriteLine (GL.GetShaderInfoLog (fragmentshader));
			GL.AttachShader (renderShader, fragmentshader);



			Stopwatch clock = new Stopwatch ();
			float now = (float)clock.Elapsed.TotalSeconds;
			float dt = now;
			clock.Start();

			Matrix4 projectionMatrix = Matrix4.CreatePerspectiveFieldOfView ((float)Math.PI * 0.25f, 800.0f / 480.0f, 0.1f, 100.0f);

			do {
				OpenTK.Graphics.OpenGL.GL.Clear (OpenTK.Graphics.OpenGL.ClearBufferMask.ColorBufferBit | OpenTK.Graphics.OpenGL.ClearBufferMask.DepthBufferBit);
				dt = (float)clock.Elapsed.TotalSeconds - now;
				now = (float)clock.Elapsed.TotalSeconds;

				Matrix4 modelViewMatrix = Matrix4.LookAt (15.0f * (float)Math.Sin (now * 0.2f), (float)(Math.Cos (now * 2.0f)), 15.0f * (float)Math.Cos (now * 0.2f), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

				Console.WriteLine("FPS : " + 1.0f/dt);

				context.SwapBuffers ();
				window.ProcessEvents ();
			} while(window.Exists && !isExiting);
		}
	}
}
