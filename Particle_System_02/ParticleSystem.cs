﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Particle_System_02
{
	public class ParticleSystem
	{
		private List<Particle> particles_ = new List<Particle>();
		private List<Emitter> emitters_ = new List<Emitter>();

		private float[] particlelist_= new float[600000];
		private uint[] particleIndices_ = new uint[200000];

		public IntPtr BufferA;
		public IntPtr BufferB;

		public IntPtr currbuffer_;

		public int vbo_ = 0;
		public int vao_ = 0;
		public int tfo_ = 0;




		public bool DomainLimitation = false;
		public float ParticleRenderSize = 1.0f;


		public ParticleSystem ()
		{
			BufferA = new IntPtr (150000 * sizeof(float) * 3);
			BufferB = new IntPtr (150000 * sizeof(float) * 3);

			tfo_ = 0;

			vao_ = GL.GenVertexArray ();
			GL.BindVertexArray (vao_);

			vbo_ = GL.GenBuffer ();
			GL.BindBuffer (BufferTarget.ArrayBuffer, vbo_);

			GL.EnableVertexAttribArray (0);
			GL.VertexAttribPointer (0, 3, VertexAttribPointerType.Float, false, 0, 0);

			GL.BindBuffer (BufferTarget.ArrayBuffer, 0);

			GL.Enable (EnableCap.PointSprite);
		}

		public void AddEmitter(Emitter emitter)
		{
			emitters_.Add (emitter);
		}

		private void AddParticles(List<Particle> particleList)
		{
			foreach (Particle p in particleList)
				particles_.Add (p);
		}

		public void Bufferize()
		{
			GL.Enable (EnableCap.Blend);
			GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrc1Alpha);
			GL.PointSize (ParticleRenderSize);
			GL.BindVertexArray (vao_);
			GL.DrawArrays (PrimitiveType.Points, 0, particles_.Count);
			GL.BindVertexArray (0);
		}

		public int GetParticlesCount()
		{
			return particles_.Count;
		}

		public int GetVAO()
		{
			return vao_;
		}



	}
}

